export class Angular2SongkickPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('angular2-songkick-app h1')).getText();
  }
}

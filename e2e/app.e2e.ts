import { Angular2SongkickPage } from './app.po';

describe('angular2-songkick App', function() {
  let page: Angular2SongkickPage;

  beforeEach(() => {
    page = new Angular2SongkickPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('angular2-songkick works!');
  });
});

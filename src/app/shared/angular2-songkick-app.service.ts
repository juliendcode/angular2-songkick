import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Event }         from './event.ts'
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Angular2SongkickAppService {
  constructor (private http: Http) {}

  private dataURL = 'http://api.songkick.com/api/3.0/artists/' + 7125659 +
                    '/calendar.json?apikey=' + 'rM0kT4HReTir83gl';  // URL to web API

  getData () {
    //return Promise.resolve(HEROES);
    return this.http.get(this.dataURL)
                    .toPromise()
                    .then(response => response.json().resultsPage.results)
                    .catch(this.handleError);
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

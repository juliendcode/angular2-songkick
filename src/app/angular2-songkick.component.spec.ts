import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { Angular2SongkickAppComponent } from '../app/angular2-songkick.component';

beforeEachProviders(() => [Angular2SongkickAppComponent]);

describe('App: Angular2Songkick', () => {
  it('should create the app',
      inject([Angular2SongkickAppComponent], (app: Angular2SongkickAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'angular2-songkick works!\'',
      inject([Angular2SongkickAppComponent], (app: Angular2SongkickAppComponent) => {
    expect(app.title).toEqual('angular2-songkick works!');
  }));
});

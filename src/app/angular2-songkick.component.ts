import { Component, OnInit }          from '@angular/core';
import { HTTP_PROVIDERS }             from '@angular/http';
import { Angular2SongkickAppService } from './shared/angular2-songkick-app.service';
import { Event }                      from './shared/event.ts'

@Component({
  moduleId: module.id,
  selector: 'angular2-songkick-app',
  templateUrl: 'angular2-songkick.component.html',
  styleUrls: ['angular2-songkick.component.css'],
  providers: [ HTTP_PROVIDERS, Angular2SongkickAppService ]
})
export class Angular2SongkickAppComponent implements OnInit {
  constructor(private _angular2SongkickAppService: Angular2SongkickAppService) { }

  events: Event[];

  ngOnInit() { this.getData(); }
  getData(){
    console.log(this._angular2SongkickAppService.getData());
  }

  title = 'angular2-songkick works!';
}
